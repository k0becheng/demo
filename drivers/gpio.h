/**
 * @file gpio.h
 * @brief  gpio驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-07
 */
 
#ifndef __GPIO_H
#define __GPIO_H

#include "stm32f10x.h"                  // Device header

struct GPIO_PIN
{
    GPIO_TypeDef *port;
    uint16_t      pin;
};

void gpio_clock_enable(GPIO_TypeDef* GPIOx);

#endif 
