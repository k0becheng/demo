﻿/**
 * @file    user_led.c
 * @brief   LED驱动程序.
 * @author  k0becheng
 * @version V0.0.0
 * @date    2014-01-28
 * @par     版本    日期        作者        修改说明
            V0.0.0  2014-01-28  qianjun     首次创建
 */
 
/** Includes ---------------------------------------------------------------- */
#include "led.h"
#include "gpio.h"

const struct GPIO_PIN led_pin[] =
{
    { GPIOD,   GPIO_Pin_2 },
    { GPIOD,   GPIO_Pin_3 },
    { GPIOD,   GPIO_Pin_4 },
    { GPIOD,   GPIO_Pin_7 },
};

#define NUM_LEDS (sizeof(led_pin)/sizeof(struct GPIO_PIN))

/**
 * @brief  配置LED控制引脚的模式.
 * @param  None
 * @retval None
 */
void led_pins_cfg( void )
{
    uint8_t n;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    for ( n = 0; n < NUM_LEDS; n++ )
    {
        gpio_clock_enable( led_pin[n].port );
        
        GPIO_InitStructure.GPIO_Pin   = led_pin[n].pin;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_Init( led_pin[n].port, &GPIO_InitStructure );
    }
}

/**
 * @brief  点亮LED.
 * @param  numb : LED编号：0-3.
 * @retval None
 */
void led_off(uint8_t num) 
{
    //GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_SET);
    GPIO_WriteBit( led_pin[num].port, led_pin[num].pin, Bit_SET );
}

/**
 * @brief  熄灭LED.
 * @param  numb : LED编号：0-3.
 * @retval None
 */
void led_on(uint8_t num) 
{
    //GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_SET);
    GPIO_WriteBit( led_pin[num].port, led_pin[num].pin, Bit_RESET );
}

/**
 * @brief 切换LED状态.
 *
 * @param val 取值范围：0x00-0x0F
 */
void leds_switch(uint32_t val) 
{
    uint8_t n;
    for ( n = 0; n < NUM_LEDS; n++ )
    {
        if ( val & ( 0x08 >> n ) ) 
        {
            led_on( n );
        }
        else
        {
            led_off( n );
        }
    }
}
