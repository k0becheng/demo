/**
 * @file    led.h
 * @brief   LED驱动头文件.
 * @author  k0becheng
 * @version V0.0.0
 * @date    2014-01-28
 * @par     版本    日期        作者        修改说明
            V0.0.0  2014-01-28  k0becheng   首次创建
 */

#ifndef __LED_H__
#define __LED_H__

/** Includes ---------------------------------------------------------------- */
#include "stm32f10x.h"

void     led_pins_cfg(void);
void     led_on(uint8_t num);
void     led_off(uint8_t num);
void     leds_switch(uint32_t val);
uint8_t  led_get_num(void);

#endif
