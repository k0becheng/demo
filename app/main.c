/**
 * @file    main.c
 * @brief   神舟IV号综合例程
 * @author  k0becheng
 * @version V0.0.0
 * @date    2014-08-26
 * @par     版本    日期        作者        修改说明
            V0.0.0  2014-08-26  k0becheng   首次创建
 */

/** Includes ---------------------------------------------------------------- */
#include "led.h"
#include "delay.h"
#include "key.h"

/**
 * @brief  主函数入口.
 * @param  None
 * @retval None
 */
int main( void )
{
    enum KEY_N key = KEY_NULL;
    uint8_t count = 0;

    led_pins_cfg(  );      /**< 规范注释 */
    key_config(  );

    while ( 1 )
    {
        key = key_get(  );
        if ( key != KEY_NULL )
        {
            count++;
            if ( count > 15 )
            {
                count = 0;
            }
        }
        leds_switch( count );
        delay( 1000000 );
        leds_switch( 0x00 );
        delay( 1000000 );
    }
}

/** 添加了一行用于测试git pull的功能 */
